import React, { useState, useEffect } from 'react';
import Header from './components/Header/Header';
import Search from './components/Search/Search';
import ContactsList from './components/ContactsList/ContactsList';

const App = () => {

    const API_URL = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com'
    const USERS_ENDPOINT = 'users.json'

    const [users, setUsers] = useState([])
    const [filteredUsers, setFilteredUsers] = useState([]);

    const sortUsersAlphabetically = (users) => {
        let sortedUsers = [...users];
        sortedUsers.sort((a, b) => {
            if (a.last_name.toLowerCase() < b.last_name.toLowerCase()) {
                return -1;
            }
            if (a.last_name.toLowerCase() > b.last_name.toLowerCase()) {
                return 1;
            }
            return 0;
        })
        return sortedUsers;
    }

    const filterUsers = (arrayToSearch, phraseToSearch) => {
        return arrayToSearch.filter(element => {
            const lastName = element.last_name.toLowerCase();
            const firstName = element.first_name.toLowerCase();
            const wholeNameFirst = `${firstName} ${lastName}`;
            const wholeNameLast = `${lastName} ${firstName}`;
            const phraseToCheck = phraseToSearch.toLowerCase();
            if (lastName.includes(phraseToCheck)
             || firstName.includes(phraseToCheck)
             || wholeNameFirst.includes(phraseToCheck)
             || wholeNameLast.includes(phraseToCheck)) {
                return true;
            }
            return false;
        })
    }

    const search = (searchTerm) => {
        setFilteredUsers(filterUsers(users, searchTerm));
    }

    const getUsers = () => {
        fetch(`${API_URL}/${USERS_ENDPOINT}`)
            .then(response => response.json())
            .then(users => {
                const sortedUsers = sortUsersAlphabetically(users);
                setUsers(sortedUsers);
                setFilteredUsers(sortedUsers);
            })
        }

    useEffect(() => {
        getUsers();
    }, [])


    return (
        <>
            <Header />
            <Search search={search}/>
            <ContactsList users={filteredUsers} />
        </>
    )
}

export default App;