import React from 'react';
import ContactsListElement from '../ContactsListElement/ContactsListElement'
import "./ContactsList.scss";

const ContactsList = ({users, getUsers}) => {

    return (
        <div className="contacts-list">
            <ul className='contacts-list'>
                {users.map((user, id) => {
                    return <ContactsListElement user={user} key={id}/>
                })}
            </ul>
        </div>
    )
}

export default ContactsList;