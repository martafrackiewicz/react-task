import React from 'react';
import "./Search.scss";

const Search = ({search}) => {

    return (
        <div className="search">
            <i className="fa fa-search search-icon"></i>
            <input type="search" onChange={e => search(e.target.value)}></input>
        </div>
    )
}

export default Search;