import React, { useState } from 'react';
import "./ContactsListElement.scss";

const ContactsListElement = ({user, id}) => {

    const [isChecked, setIsChecked] = useState(false)

    const toggleChecked = () => {
        setIsChecked(!isChecked);
        if (isChecked === false) {
            console.log('Checked user ID: ', user.id)
        }
    }

    return <li className='contacts-list-element' key={id} onClick={toggleChecked}>
        {user.avatar? <img src={user.avatar} alt='User avatar'></img> : <div className="element-avatar"></div>}
        <div className='element-text'>
            <p className='element-text-name'>{user.first_name} {user.last_name} </p>
            <p className='element-text-email'>{user.email}</p>
        </div>
        {isChecked ? <input type='checkbox' checked onChange={() => {}}></input> : null}
    </li>
              
}

export default ContactsListElement;