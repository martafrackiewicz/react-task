import React from 'react';
import "./Header.scss";

const Header = () => {
    return (
        <header className='header'>
            <p className='header-title'>Contacts</p>
        </header>
    )
}

export default Header;